const transpose = matrix => (
  matrix[0].map((ignore, index) => (
    matrix.map(row => row[index])
  ))
)

const counterTranspose = matrix => {
  return rotate90DegreesClockWise(matrix).reverse()
}

const rotate90DegreesClockWise = matrix => {
  let newMatrix = matrix.slice()
  return transpose(newMatrix.reverse())
}

const rotate90DegreesCounterClockWise = matrix => {
  let newMatrix = matrix.slice()
  return transpose(newMatrix).reverse()
}

/*
  [1, 2, 3],
  [4, 5, 6], => [1, 2, 3, 6, 9, 8, 7, 4, 5]
  [7, 8, 9]
*/
const getSpiral = matrix => {
  let newMatrix = matrix.slice()
  let spiral = []

  while (newMatrix.length > 1) {
    newMatrix[0].forEach(elem => {
      spiral.push(elem)
    })
    newMatrix.splice(0, 1)
    newMatrix = transpose(newMatrix).reverse()
  }
  spiral.push(newMatrix[0][0])
  return spiral
}

const myMatrix = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
]

console.log('Matrix:           ', myMatrix)
console.log('Transpose:        ', transpose(myMatrix))
console.log('Counter transpose:', counterTranspose(myMatrix))
console.log('+90 degrees:      ', rotate90DegreesClockWise(myMatrix))
console.log('-90 degrees:      ', rotate90DegreesCounterClockWise(myMatrix))
console.log('Spiral:           ', getSpiral(myMatrix))

/*
Matrix:            [ [ 1, 2, 3 ], [ 4, 5, 6 ], [ 7, 8, 9 ] ]
Transpose:         [ [ 1, 4, 7 ], [ 2, 5, 8 ], [ 3, 6, 9 ] ]
Counter transpose: [ [ 9, 6, 3 ], [ 8, 5, 2 ], [ 7, 4, 1 ] ]
+90 degrees:       [ [ 7, 4, 1 ], [ 8, 5, 2 ], [ 9, 6, 3 ] ]
-90 degrees:       [ [ 3, 6, 9 ], [ 2, 5, 8 ], [ 1, 4, 7 ] ]
Spiral:            [ 1, 2, 3, 6, 9, 8, 7, 4, 5 ]
*/