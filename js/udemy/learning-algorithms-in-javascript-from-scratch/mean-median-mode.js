const getMean = array => {
	let sum = 0;
	array.forEach(num => {
		sum += num;
	});
	return sum / array.length;
};

const getMedian = array => {
	array.sort((a, b) => {
	   return a - b;
	});
	let median = 0;
	if (array.length % 2 !== 0) {
		let indexOfMiddleElement = Math.floor(array.length / 2);
		median = array[indexOfMiddleElement];
	} else {
		let rightMiddle = array[array.length / 2];
		let leftMiddle = array[array.length / 2 - 1];
		median = (leftMiddle + rightMiddle) / 2;
	}
	return median;
};

const getMode = array => {
	let mode = {};
	array.forEach(num => {
		if (!mode[num]) {
			mode[num] = 1;
		} else {
			mode[num]++;
		}
	});
	let maxFrequency = 0;
	let modes = [];
	for (let num in mode) {
		if (mode[num] > maxFrequency) {
			modes = [ num ];
			maxFrequency = mode[num];
		} else if (mode[num] === maxFrequency) {
			modes.push(num);
		}
	}
	
	if (modes.length === Object.keys(mode).length) {
		modes = [];
	}
	return modes;
};

const meanMedianMode = array => {
	return {
		mean: getMean(array),
		median: getMedian(array),
		mode: getMode(array)
	}
};

console.log(meanMedianMode([1, 2, 3, 4, 5, 4, 6, 1]));
// => { mean: 3.25, median: 3.5, mode: [ '1', '4' ] }

console.log(meanMedianMode([9, 10, 23, 10, 23, 9]));
// => { mean: 14, median: 10, mode: [] }