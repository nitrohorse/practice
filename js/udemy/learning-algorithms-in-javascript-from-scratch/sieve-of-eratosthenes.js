// Return all prime numbers up to `num` in an array
const sieveOfEratosthenes = num => {
	let primes = [];
	primes.push(false);
	primes.push(false);
	
	for (let i = 2; i <= num; i++) {
		primes.push(true);
	}
	
	const squareRootOfNum = Math.sqrt(num);

	for (let i = 2; i <= squareRootOfNum; i++) {
		for (let j = 2; j * i <= num; j++) {
			primes[i * j] = false;
		}
	}

	let primeNums = [];
	for (let i = 0; i < primes.length; i++) {
		if (primes[i]) {
			primeNums.push(i);
		}
	}
	return primeNums;
};

console.log(sieveOfEratosthenes(30));
// => [2,3,5,7,11,13,17,19,23,29]