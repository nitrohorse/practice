const twoSum = (numArray, sum) => {
	
	let twoSumArr = [];
	for (let i = 0; i < numArray.length; i++) {
		for (let j = i; j < numArray.length; j++) {
			let currentNum = numArray[i];
			let nextNum = numArray[j];
			if (currentNum + nextNum === sum) {
				twoSumArr.push([currentNum, nextNum]);
			}
		}
	}
	return twoSumArr;
};

console.log(twoSum([1, 6, 4, 5, 3, 3], 7));
// => [ [ 6, 1 ], [ 3, 4 ], [ 3, 4] ]

console.log(twoSum([40, 11, 19, 17, -12], 28));
// => [ [ 40, -12 ], [ 11, 17 ] ]