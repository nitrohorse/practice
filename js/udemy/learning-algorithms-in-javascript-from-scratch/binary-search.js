const binarySearch = (numArray, key) => {
	let middleIndex = Math.floor(numArray.length / 2);
	let middleElement = numArray[middleIndex];
	
	if (middleElement === key) {
		return true;
	} 
	
	if (numArray.length > 1) {
		if (middleElement < key) {
			return binarySearch(numArray.splice(middleIndex, numArray.length), key);
		}
		if (middleElement > key) {
			return binarySearch(numArray.splice(0, middleIndex), key);
		}
	}
	
	return false;
};

console.log(binarySearch([5,7,12,16,36,39,42,56,71],5));
// => true