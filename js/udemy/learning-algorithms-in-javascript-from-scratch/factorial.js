const factorialRecursive = num => {
	if (num === 1) {
		return num;
	} else {
		return num * factorialRecursive(num - 1);
	}
};

const factorialIterative = num => {
	let factorial = 1;
	while (num >= 1) {
		factorial *= num;
		num--;
	}
	return factorial;
};

console.log(factorialRecursive(4));
// => 4! = 4 * 3 * 2 * 1 = 24

console.log(factorialIterative(4));
