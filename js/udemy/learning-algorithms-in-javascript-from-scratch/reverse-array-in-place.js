const reverseArrayInPlace = (arr) => {
	let i = 0;
	let j = arr.length - 1;
	
	while (i < j) {
		let temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
		i++;
		j--;
	}
	return arr;
}

console.log(reverseArrayInPlace([1,2,3,4,5]));
// => [5,4,3,2,1]

console.log(reverseArrayInPlace([4,5,6,7,8,9]));
// => [9,8,7,6,5,4]