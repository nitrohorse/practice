// Fibonacci Sequence: 1,1,2,3,5,8,13,21,34

// O(2^n)
const fibonacci = position => {
	// base case
    if (position < 3) {
    	return 1;
    }
    // recursive case
    return fibonacci(position - 1) + fibonacci(position - 2);
};

// O(n)
const fibonacciMemoized = (index, cache) => {
	cache = cache || [];
	// base case
	if (cache[index]) {
		return cache[index];
	}
	if (index < 3) {
		return 1;
	} else {
		// recursive case
		cache[index] = fibonacciMemoized(index - 1, cache) + fibonacciMemoized(index - 2, cache);
	}
	return cache[index];
};

console.log(fibonacci(9));
// => 34
console.log(fibonacciMemoized(20));
// > 6765
console.log(fibonacciMemoized(9));
console.log(fibonacciMemoized(20));
