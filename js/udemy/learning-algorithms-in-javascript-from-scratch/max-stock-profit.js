// Take in array of prices as parameter
// Return the max possible profit of the day
// Return -1 if no profit possible
// Max profit of 0 is treated as any other max profit value
const maxStockProfit = pricesArr => {
	let maxProfit = -1;
	let buyPrice = 0;
	let sellPrice = 0;
	let changeBuyPrice = true;
	
	for (let index = 0; index < pricesArr.length; index++) {
		if (changeBuyPrice) {
			buyPrice = pricesArr[index];
		}
		sellPrice = pricesArr[index + 1];
		
		if (sellPrice < buyPrice) {
			changeBuyPrice = true;
		} else {
			let tempProfit = sellPrice - buyPrice;
			if (tempProfit > maxProfit) {
				maxProfit = tempProfit;
			}
			changeBuyPrice = false;
		}
	}
	return maxProfit;
};

console.log(maxStockProfit([32,46,26,38,40,48,42]));
// => 22 (buy price: 26, sell price: 48)

console.log(maxStockProfit([10,18,4,5,9,6,16,12]));
// => 12 (buy price: 4, sell price: 16)