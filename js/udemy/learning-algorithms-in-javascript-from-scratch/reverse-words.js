const reverseWords = (string) => {
	let words = string.split(' ');
	let reverse = [];
	
	words.forEach(word => {
	   let reversedWord = '';
	   for (let index = word.length - 1; index >= 0; index--) {
		   let letter = word[index];
		   reversedWord += letter;
	   }
	   reverse.push(reversedWord);
	});
	return reverse;
};

reverseWords('this is a string of words');
// => 'siht si a gnirts fo sdrow'