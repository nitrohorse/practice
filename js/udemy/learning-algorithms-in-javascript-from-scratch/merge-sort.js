// Take in a single, unsorted array
// Split the array into two halves
const mergeSort = array => {
	// base case
	if (array.length < 2) {
		return array;
	}
	// recursive case
	let middleIndex = Math.floor(array.length / 2);
	let firstHalf = array.slice(0, middleIndex);
	let secondHalf = array.slice(middleIndex);
	
	return merge(mergeSort(firstHalf), mergeSort(secondHalf));
};

// Take in two sorted arrays
// Merge those sorted arrays into one sorted array
const merge = (array1, array2) => {
	let merged = [];

	while (array1.length && array2.length) {
		let minElement = 0;
		if (array1[0] < array2[0]) {
			minElement = array1.shift();
		} else {
			minElement = array2.shift();
		}
		merged.push(minElement);
	}
	
	if (array1.length) {
		merged = merged.concat(array1);
	} else {
		merged = merged.concat(array2);
	}
	return merged;
};

console.log(mergeSort([11,7,4,1,15,12,3]));
// => [1,3,4,7,11,12,15]