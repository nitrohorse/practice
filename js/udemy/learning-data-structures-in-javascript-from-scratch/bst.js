class BST {
  constructor (value) {
    this.value = value
    this.left = null
    this.right = null
  }

  // O(log(n))
  insert (value) {
    if (value <= this.value) {
      if (!this.left) {
        this.left = new BST(value)
      } else {
        this.left.insert(value)
      }
    } else {
      if (!this.right) {
        this.right = new BST(value)
      } else {
        this.right.insert(value)
      }
    }
  }

  // O(log(n))
  contains (value) {
    if (value === this.value) {
      return true
    } else if (value < this.value) {
      if (!this.left) {
        return false;
      } else {
        return this.left.contains(value)
      }
    } else if (value > this.value) {
      if (!this.right) {
        return false;
      } else {
        return this.right.contains(value)
      }
    }
  }

  // O(n)
  depthFirstTraversal (iteratorFunc, order) {
    if (order === 'pre-order') {
      iteratorFunc(this.value)
    }
    if (this.left) {
      this.left.depthFirstTraversal(iteratorFunc, order)
    }
    if (order === 'in-order') {
      iteratorFunc(this.value)
    }
    if (this.right) {
      this.right.depthFirstTraversal(iteratorFunc, order)
    }
    if (order === 'post-order') {
      iteratorFunc(this.value)
    }
  }

  // O(n)
  breadthFirstTraversal (iteratorFunc) {
    let queue = [this]

    while (queue.length) {
      const treeNode = queue.shift()
      iteratorFunc(treeNode.value)
      if (treeNode.left) {
        queue.push(treeNode.left)
      }
      if (treeNode.right) {
        queue.push(treeNode.right)
      }
    }
  }

  // O(n)
  getMinVal () {
    if (this.left) {
      return this.left.getMinVal()
    } else {
      return this.value
    }
  }

  // O(n)
  getMaxVal() {
    if (this.right) {
      return this.right.getMaxVal()
    } else {
      return this.value
    }
  }
}

const log = value => {
   console.log(value + '->')
}

let bst = new BST(50)
bst.insert(8)
bst.insert(51)
bst.insert(100)
bst.insert(42)
bst.insert(1)
console.log(bst.contains(9))
console.log(bst)
console.log(bst.depthFirstTraversal(log, 'in-order'))
console.log(bst.breadthFirstTraversal(log))
console.log(bst.getMinVal())
console.log(bst.getMaxVal())