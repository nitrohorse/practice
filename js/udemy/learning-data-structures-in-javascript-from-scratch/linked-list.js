class Node {
	constructor(value, next, prev) {
		this.value = value;
		this.next = next;
		this.prev = prev;
	}
}

class LinkedList {
	constructor() {
		this.head = null;
		this.tail = null;
	}

	// O(1)
	addToHead(value) {
		const newNode = new Node(value, this.head, null);

		if (this.head) {
			this.head.prev = newNode;
		} else {
			this.tail = newNode;
		}
		this.head = newNode;
	}

	// O(1)
	addToTail(value) {
		const newNode = new Node(value, null, this.tail);

		if (this.tail) {
			this.tail.next = newNode;
		} else {
			this.head = newNode;
		}
		this.tail = newNode;
	}

	// O(1)
	removeHead() {
		if (!this.head) {
			return null;
		} else {
			const headVal = this.head.value;

			this.head = this.head.next;
			if (this.head) {
				this.head.prev = null;
			} else {
				this.tail = null;
			}
			return headVal;
		}
	}

	// O(1)
	removeTail() {
		if (!this.tail) {
			return null;
		} else {
			const tailVal = this.tail.value;

			this.tail = this.tail.prev;			
			if (this.tail) {
				this.tail.next = null;
			} else {
				this.head = null;
			}
			return tailVal;
		}
	}

	// O(n)
	search(value) {
		if (!this.head) {
			return null;
		} else {
			let currentNode = this.head;

			while (currentNode) {
				if (currentNode.value === value) {
					return currentNode.value;
				}
				currentNode = currentNode.next;
			}
			return null;
		}
	}

	// O(n)
	// LinkedList => 3->5->3->8
	// indexOf(3) returns [0,2]
	indexOf(value) {
		let indexes = [];
		let index = 0;		
		let currentNode = this.head;

		while (currentNode) {
			if (currentNode.value === value) {
				indexes.push(index);
			}
			currentNode = currentNode.next;
			index++;
		}
		return indexes;
	}
}

let ll = new LinkedList();
ll.addToHead(100);
// => 100
ll.addToTail(20);
// => 100->20
ll.removeHead();
// => 20
ll.removeTail();
// =>
ll.addToHead(42);
// => 42
ll.addToHead(42);
// => 42->42
ll.addToHead(100);
// => 100->42->42
ll.addToTail(12);
// => 100->42->42->12
console.log(ll);
console.log(ll.indexOf(42));
// => [ 1, 2 ]
console.log(ll.search(12));
// => 12
