class HashTable {
  constructor (size) {
    this.buckets = Array(size)
    this.numBuckets = this.buckets.length
  }

  hash (key) {
    let total = 0

    for (let i = 0; i < key.length; i++) {
      total += key.charCodeAt(i)
    }

    const bucket = total % this.numBuckets
    return bucket
  }

  insert (key, value) {
    const index = this.hash(key)

    if (!this.buckets[index]) {
      this.buckets[index] = new HashNode(key, value)
    } else if (this.buckets[index].key === key) {
      this.buckets[index].value = value
    } else {
      let currentNode = this.buckets[index]

      while (currentNode.next) {
        if (currentNode.next.key === key) {
          currentNode.next.value = value
          return;
        }
        currentNode = currentNode.next
      }
      currentNode.next = new HashNode(key, value)
    }
  }

  get (key) {
    const index = this.hash(key)

    if (!this.buckets[index]) {
      return null
    } else {
      let currentNode = this.buckets[index]

      while (currentNode) {
        if (currentNode.key === key) {
          return currentNode.value
        }
        currentNode = currentNode.next
      }
      return null
    }
  }

  // Return array of all hash nodes
  retrieveAll () {
    let allNodes = []
    for (let i = 0; i < this.numBuckets; i++) {
      let currentNode = this.buckets[i]
      while (currentNode) {
        allNodes.push(currentNode)
        currentNode = currentNode.next
      }
    }
    return allNodes
  }
}

class HashNode {
  constructor (key, value, next) {
    this.key = key
    this.value = value
    this.next = next || null
  }
}

let ht = new HashTable(30)
// console.log(ht)

ht.insert('Dean', 'dean@protonmail.com')
ht.insert('Megan', 'mega@protonmail.com')
ht.insert('Dane', 'dane@tutamail.com')
ht.insert('Dean', 'deanmachine@protonmail.com')
ht.insert('Megan', 'megansmith@protonmail.com')
ht.insert('Dane', 'dane1010@torproject.org')
ht.insert('Joe', 'joey@eff.org')
ht.insert('Samantha', 'sammy@privacy.com')

// console.log(ht)
console.log(ht.retrieveAll())