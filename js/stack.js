// https://github.com/thejameskyle/itsy-bitsy-data-structures/blob/master/itsy-bitsy-data-structures.js
class Stack {
	constructor() {
		this.list = [];
		this.length = 0;
	}

	push(value) {
		this.length++;
		this.list.push(value);
	}

	pop() {
		if (this.length === 0) return;

		this.length--;
		return this.list.pop();
	}

	peek() {
		return this.list[this.length - 1];
	}
}

let s = new Stack();
s.push(1);
s.push(2);
s.push(3);
console.log(s);
s.pop();
s.pop();
console.log(s);
console.log(s.peek());