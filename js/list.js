// https://github.com/thejameskyle/itsy-bitsy-data-structures/blob/master/itsy-bitsy-data-structures.js
class List {
	constructor() {
		this.memory = [];
		this.length = 0;
	}

	// O(n)
	get(address) {
		return this.memory[address];
	}

	// O(1)
	push(value) {
		this.memory[this.length] = value;
		this.length++;
	}

	// O(1)
	pop() {
		if (this.length === 0) return;

		let lastAddress = this.length - 1;
		let value = this.memory[lastAddress];
		delete this.memory[lastAddress];
		this.length--;

		return value;
	}

	// O(n)
	unshift(value) {
		let previous = value;

		for (let address = 0; address < this.length; address++) {
			let current = this.memory[address];
			this.memory[address] = previous;
			previous = current;
		}

		this.memory[this.length] = previous;
		this.length++;
	}

	// O(n)
	shift() {
		if (this.length === 0) return;

		let value = this.memory[0];

		for (let address = 0; address < this.length; address++) {
			this.memory[address] = this.memory[address + 1];
		}
		delete this.memory[this.length - 1];
		this.length--;

		return value;
	}
}

let list = new List();
list.push(1);
list.push(2);
list.push(3);
console.log(list);
list.unshift(0);
console.log(list);
list.shift();
console.log(list);