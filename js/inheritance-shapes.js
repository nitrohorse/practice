class Grid {
	constructor() {
		this.grid = new Array(24)
		for (let row = 0; row < 24; row++) {
			this.grid[row] = new Array(60)
			for (let col = 0; col < 60; col++) {
				this.grid[row][col] = '.'
			}
		}
	}

	set(x, y, char) {
		if (this.isWithinBounds) {
			this.x = x
			this.y = y
			this.char = char
			this.grid[this.y][this.x] = this.char
		}
	}

	print() {
		for (let row = 0; row < 24; row++) {
			let gridString = ''
			for (let col = 0; col < 60; col++) {
				gridString += this.grid[row][col]
			}
			console.log(gridString)
		}
	}

	isWithinBounds(x, y) {
		if ((x < 0 || x > 59) || (y < 0 || y > 23)) {
			return true
		} else {
			return false
		}
	}
}

class Shape {
	constructor(x, y) {
		console.log('Shape instantiated.')
		this.x = x
		this.y = y
	}
}

class Circle extends Shape {
	constructor(x, y) {
		super(x, y)
		console.log('Circle instantiated.')
	}

	draw(grid) {
		for (let row = 0; row < 4; row++) {
			for (let col = 0; col < 4; col++) {
				if (((row == 0 || row == 3) && (col == 1 || col == 2)) 
				|| ((row == 1 || row == 2) && (col == 0 || col == 3))) {
					grid.set(col + this.x, row + this.y, 'o')
				}
			}
		}
	}
}

class Square extends Shape {
	constructor(x, y, size) {
		super(x, y)
		this.size = size
		console.log('Square instantiated.')
	}

	draw(grid) {
		for (let row = 0; row < this.size; row++) {
			for (let col = 0; col < this.size; col++) {
				if ((row == 0 || row == (this.size - 1))
				|| col == 0 || col == (this.size - 1)) {
					grid.set(row + this.x, col + this.y, '*')
				}
			}
		}
	}
}

class Triangle extends Shape {
	constructor(x, y) {
		super(x, y)
		console.log('Triangle instantiated.')
	}

	draw(grid) {
		for (let row = 0; row < 3; row++) {
			for (let col = 0; col < 5; col++) {
				if ((row == 0 && col == 2) 
				|| (row == 1 && (col == 1 
				|| col == 3)) || (row == 2 && col < 5)) {
					grid.set(col + this.x, row + this.y, '+')
				}
			}
		}
	}
}

class Point extends Shape {
	constructor(x, y) {
		super(x, y)
		console.log('Point instantiated.')
	}

	draw(grid) {
		grid.set(this.x, this.y, '#')
	}
}

let grid = new Grid()

let shapes = []
shapes.push(new Circle(3, 4))
shapes.push(new Square(12, 9, 7))
shapes.push(new Triangle(20, 20))
shapes.push(new Point(17, 11))
console.log(shapes)

for (let index = 0; index < shapes.length; index++) {
	shapes[index].draw(grid)
}

grid.print()

/*
Shape instantiated.
Circle instantiated.
Shape instantiated.
Square instantiated.
Shape instantiated.
Triangle instantiated.
Shape instantiated.
Point instantiated.
[ Circle { x: 3, y: 4 },
  Square { x: 12, y: 9, size: 7 },
  Triangle { x: 20, y: 20 },
  Point { x: 17, y: 11 } ]
............................................................
............................................................
............................................................
............................................................
....oo......................................................
...o..o.....................................................
...o..o.....................................................
....oo......................................................
............................................................
............*******.........................................
............*.....*.........................................
............*....#*.........................................
............*.....*.........................................
............*.....*.........................................
............*.....*.........................................
............*******.........................................
............................................................
............................................................
............................................................
............................................................
......................+.....................................
.....................+.+....................................
....................+++++...................................
............................................................
*/