// https://github.com/thejameskyle/itsy-bitsy-data-structures/blob/master/itsy-bitsy-data-structures.js
class HashTable {
	constructor() {
		this.memory = [];
	}

	hashKey(key) {
		let hash = 0;
		for (let index = 0; index < key.length; index++) {
			// Oh look– magic.
			let code = key.charCodeAt(index);
			hash = ((hash << 5) - hash) + code | 0;
		}
		return hash;
	}

	// O(1)
	get(key) {
		let address = this.hashKey(key);
		return this.memory[address];
	}

	// O(1)
	set(key, value) {
		let address = this.hashKey(key);
		this.memory[address] = value;
	}

	// O(1)
	remove(key) {
		let address = this.hashKey(key);
		if (this.memory[address]) {
			delete this.memory[address];
		}
	}
}

let ht = new HashTable();
console.log(ht.hashKey('A'));
ht.set('A', 'Alaska');
console.log(ht.get('A'));